<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyBetweenManufacturersAndMonitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('monitors', function (Blueprint $table) {
            $table->integer('manufacturer_id')
                ->unsigned()
                ->change();

            $table->foreign('manufacturer_id')
                ->references('id')
                ->on('manufacturers');
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('monitors', function (Blueprint $table) {
            $table->dropForeign('manufacturer_id');

            $table->integer('manufacturer_id')
                ->change();
        });

        Schema::enableForeignKeyConstraints();
    }
}
