<?php

namespace App\Http\Controllers;

use App\Manufacturer;
use Illuminate\Http\Request;

class NotDefaultController extends Controller
{
    public function index()
    {
        $manufacturers = Manufacturer::all();

        return view('manufacturers', [
            'manufacturers' => $manufacturers
        ]);
    }

    public function show($id)
    {
        $manufacturer = Manufacturer::findOrFail($id);

    }

    public function create()
    {
        return view('manufacturers.create');
    }

    public function store(Request $request)
    {
        $title = $request->get('title');

        $manufacturer = new Manufacturer();
        $manufacturer->title = $title;

        $manufacturer->save();

        return redirect()
            ->to('/manufacturers');
    }

    public function edit($id)
    {
        $manufacturer = Manufacturer::findOrFail($id);

        $manufacturer->save();

        return view('manufacturers.edit', [
            'manufacturer' => $manufacturer
        ]);
    }

    public function update(Request $request, $id)
    {
        $title = $request->get('title');
//        $manufacturers = new Manufacturer()
        $manufacturer = Manufacturer::findOrFail($id);
        $manufacturer->title = $title;

        $manufacturer->save();

        return redirect()->to('/manufacturers');
    }
}
